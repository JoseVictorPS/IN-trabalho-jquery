$(document).ready(function(){

    $.post("https://jquery-19-1.herokuapp.com/destinations",{
        contentType: 'json',
        destination: {
            name: 'Jose',
            description: 'Aleluia irmão',
            more_info: 'Testing',
            price: '123'
        },
        function(data, status){
            alert("Data: " + data + "\nStatus: " + status);
        }
    });

    $.getJSON("https://jquery-19-1.herokuapp.com/destinations",function(data){
        var tabela_dados = '';
        $.each(data,function(key, value){
            tabela_dados += '<tr>';
            tabela_dados += '<td>' + value.name + '</td>';
            tabela_dados += '<td>' + value.description + '</td>';
            tabela_dados += '<td>' + value.more_info + '</td>';
            tabela_dados += '<td>' + value.price + '</td>';
            tabela_dados += '</tr>';
        });
        $('#tabela').append(tabela_dados);
    });
});