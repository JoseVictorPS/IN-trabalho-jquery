$(document).ready(function(){

    $("#cpf").mask("000.000.000-00")
    $("#telefone").mask("(00)00000-0000")
    $("#cep").mask("00.000-000")

    var options = {
        translation: {
            'A' : {pattern: /[A-Z]/},
            'a' : {pattern: /[a-z]/}
        }
    }

    $("#codigo").mask("AA0.aaA00-aA00", options)

    $("form").submit(function(event) {

        var nameValid = false;

        var name = $("input[name=nome]").val().trim();

        if(name.length>8) {
            nameValid = true;
        }

        var cpfValid = false;

        var cpf = $("input[name=cpf]").val().trim();

        if(cpf.length==14) {
            cpfValid = true;
        }

        var telValid = false;

        var tel = $("input[name=telefone]").val().trim();

        if(tel.length == 14) {
            telValid = true;
        }

        var cepValid = false;

        var cep = $("input[name=cep]").val().trim();

        if(cep.length == 10) {
            cepValid = true;
        }

        var codValid = false;

        var cod = $("input[name=codigo]").val().trim();

        if(cod.length == 14) {
            codValid = true;
        }

        if(cpfValid && nameValid && telValid){
            alert("Formulario Válido!");
        }
        else {
            event.preventDefault();
            alert("Não foi validado!")
        }

    });

});